
### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

apt update

apt install -y git

git clone https://gitlab.freedesktop.org/mesa/mesa.git
cd mesa

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update

#Z#
apt build-dep -y mesa

meson build/

ninja -C build/

```

